﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public enum AtkAnimState { IDLE, DRAW, THROW, MELEE, RECOVERING }

/// <summary>
/// Player animation controller that can simultaneously control moustache and arm
/// </summary>
public class PlayerAnim : MonoBehaviour {
	public Animator anim;  // player animator
	public Animator moAnim;  // moustache animator
	public Animator armAnim;
	public Moustache moustache;
	public PlayerMelee melee;
	private AtkAnimState state = AtkAnimState.IDLE;
	public PlayerSFX playerSFX;
	public AudioClip[] fwipSFX;
	public Image flashOverlay;
	
	bool flashing = false;
	bool successing = false;
	public AudioSource audio;
	bool IsReady { get { return state == AtkAnimState.IDLE; } }
	AudioClip Fwip { get { return fwipSFX[Random.Range(0, fwipSFX.Length)]; } }
	
		
	void Start() {
		audio = GetComponent<AudioSource>();
	}
	
	public IEnumerator DrawMoustache() {
		if (!IsReady) yield break;
		state = AtkAnimState.DRAW;
		audio.PlayOneShot(playerSFX.Effort);
		
		// start the arm draw anim
		armAnim.SetTrigger ("draw");
		
		// the anim pulls off the mo at ~ 11th frame
		for (int i = 0; i < 11; i++) 
			yield return new WaitForSeconds(Time.deltaTime);
			
		audio.PlayOneShot(playerSFX.Effort);
		moustache.ToggleDummy(true);
		
		// wait a few more frames for the arm to come to rest
		for (int i = 0; i < 8; i++) 
			yield return new WaitForSeconds(Time.deltaTime);
			
		// set the mo state here
		moustache.state = ThrownState.ARMED;
		state = AtkAnimState.IDLE;
	}
	
	public IEnumerator ThrowMoustache(Vector3 dir) {
		if (!IsReady) yield break;
		state = AtkAnimState.THROW;
		
		audio.PlayOneShot(playerSFX.Effort);
		
		// start the arm draw anim
		armAnim.SetTrigger ("throw");
		
		// throw frame = 10
		for (int i = 0; i < 14; i++) 
			yield return new WaitForSeconds(Time.deltaTime);
		
		moustache.Launch (dir);
		state = AtkAnimState.IDLE;
	}
	
	public IEnumerator Melee() {
		if (!IsReady) yield break;
		state = AtkAnimState.MELEE;
		
		// start the arm draw anim
		armAnim.SetTrigger ("melee");
		
		BoxCollider col = moustache.IsArmed ?  melee.hitboxLarge : melee.hitboxSmall;
		
		// start hitbox at frame 4
		yield return new WaitForSeconds(9 * Time.deltaTime);
		
		audio.PlayOneShot(Fwip);
		audio.PlayOneShot(playerSFX.Effort);
		col.enabled = true;
		
		// disable at frame 7
		yield return new WaitForSeconds(17 * Time.deltaTime);
		
		col.enabled = false;
		
		state = AtkAnimState.IDLE;
	}
	
	public IEnumerator Success() {
		if (successing) yield break;
		successing = true;
		yield return new WaitForSeconds(.4f);
		audio.PlayOneShot(playerSFX.KillConfirm);
		yield return null;
		successing = false;
	}
	
	public IEnumerator GotHit(UnityAction recovered) {
		AtkAnimState prevState = state;
		state = AtkAnimState.RECOVERING;
		
		audio.PlayOneShot(playerSFX.Effort);
		
		StartCoroutine(FlashScreen (.4f));
		
		yield return new WaitForSeconds(Time.deltaTime * 2);
		
		if (recovered != null) recovered();
		
		state = prevState;
	}
	
	IEnumerator FlashScreen(float duration) {
		if (flashing) yield break;
		flashing = true;
		
		for (float elapsed = 0; 
		     elapsed <= duration;
		     elapsed += Time.deltaTime) 
		{
			flashOverlay.gameObject.SetActive(!flashOverlay.gameObject.activeSelf);
			yield return new WaitForSeconds(2 * Time.deltaTime);
		}
		
		flashOverlay.gameObject.SetActive(false);
		
		flashing = false;
	}
}

[System.Serializable]
public struct PlayerSFX {
	[SerializeField] AudioClip[] effort;
	[SerializeField] AudioClip[] pain;
	[SerializeField] AudioClip[] taunt;
	[SerializeField] AudioClip[] joke;
	[SerializeField] AudioClip[] killConfirm;
	
	// Properties to get random in arrays
	public AudioClip Effort {
		get {
			return effort[Random.Range(0, effort.Length)];
		}
	}
	public AudioClip Pain {
		get {
			return pain[Random.Range(0, pain.Length)];
		}
	}
	public AudioClip Taunt {
		get {
			return taunt[Random.Range(0, taunt.Length)];
		}
	}
	public AudioClip Joke {
		get {
			return joke[Random.Range(0, joke.Length)];
		}
	}
	public AudioClip KillConfirm {
		get {
			return killConfirm[Random.Range(0, killConfirm.Length)];
		}
	}
}
