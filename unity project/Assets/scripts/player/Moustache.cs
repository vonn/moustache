﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public enum ThrownState { IDLE, ARMED, FLYING, RETURNING, STUCK }

/// <summary>
/// Controls movement of the moustache
/// </summary>
public class Moustache : MonoBehaviour 
{
	public ThrownState state = ThrownState.IDLE;
	public Player source;  // Where to return to after flight
	public GameObject dummyMoustache;  // to show arm holding me
	public GameObject hitFX;  // what to play on hit
	public AudioClip flyingSFX;  // played in flight
	public Vector3 startingPoint;  // where I was launched from
	public int damage = 1;  // damage to inflict on impact
	public float distance = 5.0f;  // how far to fly
	public float flightDuration = 1.2f;  // how long til i start returning
	public float returnDuration = 1.2f;  // how long it'll try to take to return to source
	public bool boomerang = true;  // Whether i should return to source or nah
	public bool sticky = false;  // Am I able to get stuck?
	public bool penetration = false;  // Can I go through enemies?
	float returnTime = 0;  // When should i start returning
	float launchTime = 0;  // When was I launched?
	Animator anim;

	public bool IsArmed { get{ return state == ThrownState.ARMED; } }

	
#region UNITY METHODS
	void Start() {
		anim = GetComponent<Animator>();
		ObjectPool.CreatePool(hitFX, 4);
	}
	
	void OnTriggerEnter(Collider col) {
		//print ("moustache hit a thing: " + col.name);
		if (col.transform.GetComponentInChildren<Player>() == source) return;
		
		bool either = false;  // for things that should get trigger on either flight or return
		ThrownState hitState = state;  // which state was I in when hit occurred
		
		// Hit something en route
		if (state == ThrownState.FLYING) {
			either = true;
			
			// bounce back
			if (!penetration) state = ThrownState.RETURNING;
		}
		// Hit something on the backswing
		else if (state == ThrownState.RETURNING) {
			either = true;
		}
		
		if (either) {
			// give a 'stash
			if (col.CompareTag("Enemy")) {			
				Enemy wimpy = col.GetComponentInParent<Enemy>();
				
				/// only work if hit hitbox
				if (col == wimpy.hitbox) {
					Flourish(wimpy.dummyMoustache.transform.parent);
					wimpy.ApplyDamage(damage, Vector3.zero, hitState == ThrownState.RETURNING);
				}
				else {
					wimpy.ApplyDamage(0, Vector3.zero, hitState == ThrownState.RETURNING);
				}
			}
		}
	}
#endregion
	
	
#region MO
	public bool CanDraw() {
		return (state == ThrownState.IDLE);
	}
	
	public bool CanThrow() {
		return (state == ThrownState.ARMED);
	}

	public void Launch(Vector3 dir) {
		// ignore if we aren't even armed for launch
		if (state != ThrownState.ARMED) return;
		
		// update times and state
		launchTime = Time.time;
		returnTime = launchTime + flightDuration;
		
		StartCoroutine(DoTheDamnFlight(dir));
	}
	
	public Moustache DrawMoustache() {
		ToggleDummy();
		return this;
	}
	
	public void ToggleDummy(bool overrideArmed = false) {
		bool tgl = !dummyMoustache.activeSelf;
					
		dummyMoustache.SetActive(tgl);
		GetComponent<MeshRenderer>().enabled = !tgl;
		
		if (tgl && !overrideArmed) 
			state = ThrownState.ARMED;
	}
	
	/// <summary>
	/// Does the damn flight.
	/// </summary>
	/// <returns>The the damn flight.</returns>
	/// <param name="dir">Direction to fly in</param>
	IEnumerator DoTheDamnFlight(Vector3 dir) {
		Vector3 startingLocalPos = transform.parent.localPosition;
		Vector3 startingLocalRot = transform.parent.localEulerAngles;
		startingPoint = dummyMoustache.transform.parent.position;
		Vector3 dest = startingPoint + (dir.normalized * distance);
		
		ToggleDummy();
		// TODO play sfx for flying away
		AudioSource audio = GetComponent<AudioSource>();
		audio.Play();
		anim.Play("spin_cw");
		
		DetachFromSource();
		
		// FLYING
		state = ThrownState.FLYING;
		for (float elapsed = 0; 
		     (state == ThrownState.FLYING && elapsed < flightDuration); // let this be short-circuited if state is changed
		     elapsed += Time.fixedDeltaTime) 
		{
			Vector3 p = Vector3.Lerp (startingPoint, dest, Util.Ease.Out(elapsed/flightDuration) );
			transform.parent.position = p;
			yield return null;
		}
		
		// record the zenith for lerpin'
		Vector3 zenithPos = transform.parent.position;
		Quaternion zenithRot = transform.parent.rotation;
		
		// TODO maybe play different sfx for returning
		// I'd be stuck if i hit something worth sticking to along the way out
		if (state == ThrownState.STUCK) {
			// TODO: panic
			anim.Play("still");
		}
		else if (boomerang){
			state = ThrownState.RETURNING;
		
			// RETURNING
			for (float elapsed = 0; 
			     (state == ThrownState.RETURNING && elapsed < returnDuration); // let this be short-circuited if state is changed
			     elapsed += Time.fixedDeltaTime) 
			{
				transform.parent.position = Vector3.Lerp (zenithPos, source.transform.position, Util.Ease.Exp(elapsed/returnDuration, 2) );
				yield return null;
			}
			
			// I'd be stuck here if i hit something on the way back
			if (state == ThrownState.STUCK) {
				// TODO: panic
			}
			else {
				// TODO play catching sfx
				audio.Stop ();
				// snap to my source (I should be there by now)
				AttachToSource(startingLocalPos, startingLocalRot);
				anim.Play ("catch");
			}
		}
	}
	
	void DetachFromSource() {
		// relieve myself of parent
		transform.parent.parent = null;
		// TODO maybe more logic here
	}
	
	void AttachToSource(Vector3 offset, Vector3 rot) {
		// parent myself to source
		transform.parent.parent = source.transform;
		// restore offset
		transform.parent.localPosition = offset;
		transform.parent.localEulerAngles = rot;
		state = ThrownState.IDLE;
	}
	
	// spawns a flourish where the mo has hit
	public void Flourish(Transform tar) {
		GameObject fx = ObjectPool.Spawn (hitFX, transform.position);
		
		if (tar != null)
			fx.transform.parent = tar;
		else	
			fx.transform.parent = null;
		
		StartCoroutine(Util.RecycleTimeout(fx, 5));
	}
#endregion
	
}
