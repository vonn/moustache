using UnityEngine;
using System.Collections;

public class PlayerMelee : MonoBehaviour 
{
	public int BaseDamage = 1;
	public  int DamageMult = 1;
	public int Damage{ get{ return BaseDamage * DamageMult; } }
	public float MaxDistance = 1.0f;
	public BoxCollider hitboxSmall;  // unarmed melee
	public BoxCollider hitboxLarge;  // armed melee
	
	Player player;
	
	void Start() {
		player = GetComponentInParent<Player>();
	}
	
	void Update() {
		// Update damage
		if (!player.mo.IsArmed) DamageMult = 0; // unarmed is just a knockback
		else DamageMult = 1;
	}
	
	void OnTriggerEnter(Collider col) {
		// is enemy
		if (Util.IsHitbox(col) && Util.IsEnemy(col)) {
			Enemy wimpy = col.GetComponentInParent<Enemy>();
			wimpy.ApplyDamage (Damage, player.transform.position);
		}
	}
}
