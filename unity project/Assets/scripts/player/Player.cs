﻿using UnityEngine;
using System.Collections;

public enum PlayerState {
	IDLE,
	DYING,
	DEAD
}

public class Player : MonoBehaviour {
	public int hp = 1;
	public PlayerAnim p_anim; // set inspector
	public Moustache mo;
	public PlayerState state = PlayerState.IDLE;
	public bool IsAlive { get { return state != PlayerState.DEAD; } }
	
	CharacterController cc {
		get {
			return transform.parent.GetComponent<CharacterController>();
		}
	}
	// TODO not too proud of this
	public bool Controllable {
		get {
			return GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().controllable;
		}
		set {
			GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().controllable = value;
		}
	}
	static Player _instance;
	public static Player Instance { get { return _instance; } }
	GameController gc;
	public bool stunned;
	
#region Input Properties
	public bool GetFire1Down {
		get {
			return Input.GetButtonDown ("Fire1");
		}
	}

	public bool GetFire2Down {
		get {
			return Input.GetButtonDown ("Fire2");
		}
	}
	
	public bool GetFire3Down {
		get {
			return Input.GetButtonDown ("Fire3");
		}
	}
#endregion
	
	
#region Unity Methods
	void Awake() {
		_instance = this;
	}

	void Start() {
		// TODO move this outside of player code
		gc = GameController.Instance;
	}

	void Update() {
		// update anim states
		p_anim.armAnim.SetBool("armed", mo.IsArmed);
		
		// make sure i'm not controllable if i'm stunned
		Controllable = !stunned && (IsAlive && gc.state == GameState.RUNNING);
	
		if (Controllable)
			HandleInput();
		
		p_anim.armAnim.SetFloat("hspeed", cc.velocity.sqrMagnitude);
	}
#endregion


#region Member Methods
	public bool CanMelee() {
		// if i'm armed or idle (or returning??)
		return (mo.state == ThrownState.ARMED || mo.state == ThrownState.IDLE || mo.state == ThrownState.FLYING);
	}

	void DrawMo() {
		StartCoroutine(
			p_anim.DrawMoustache()
		);
	}
	
	void ThrowMo() {
		//Vector3 vel = cc.velocity.normalized;
		//print (vel);
		//Vector3 dir = (Vector3.forward + vel).normalized;
		Vector3 dir = Vector3.forward;
		StartCoroutine(
			p_anim.ThrowMoustache(transform.TransformDirection(dir))
		);
	}
	
	void DoMelee() {
		StartCoroutine(
			p_anim.Melee()
		);
	}
	
	void HandleInput() {
		/* take input */
		// DRAW MOUSTACHE
		if (mo.CanDraw() && GetFire2Down) {
			DrawMo();
		}
		else if (mo.IsArmed) {
			// sheath mo?
		}
		// LAUNCH MOUSTACHE
		if (mo.CanThrow() && GetFire2Down) {
			ThrowMo ();
		}
		// MELEE
		if (CanMelee() && GetFire1Down) {
			DoMelee();
		}
	}
	
	// Called when player gets a kill
	public void AwardKill(Enemy victim) {
		StartCoroutine(p_anim.Success());
		
		if (victim.type == EnemyType.BIG)
			gc.stats.bigWimpsKilled++;
		else
			gc.stats.smallWimpsKilled++;
	}
	
	public void ApplyDamage(int dmg, Enemy source) {
		if (!IsAlive) return;
		StartCoroutine(Impact(source.transform.position));
		
		hp -= dmg;
		
		//FlashScreen(Color.red);
		StartCoroutine(p_anim.GotHit(null));
		
		if (hp <= 0) {
			Die();
			hp = 0;
		}
	}
	
	IEnumerator Impact(Vector3 srcPos) {
		if (stunned) yield break;
		stunned = true;
		
		int numFrames = 10; // number of frames to be stunned for
		float kbForce = 25.0f;
		
		// which direction to kb
		Vector3 dir;
		if (srcPos != Vector3.zero)
			dir = -transform.forward;
		else {
			srcPos.y = 0;
			dir = -(srcPos - transform.position).normalized;
		}
		if (dir.y < 0) dir.y = -dir.y;
		
		GetComponent<CharacterImpact>().Knockback(dir, kbForce);
		
		// stun for 10 frames;
		yield return new WaitForSeconds(numFrames * Time.deltaTime);
		
		stunned = false;
	}
	
	void Die() {
		// let the gamecontroller know it's gameover
		
		state = PlayerState.DEAD; // TODO go dying so we can play death animation
	}	
#endregion
}
