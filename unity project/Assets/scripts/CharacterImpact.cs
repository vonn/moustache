﻿using UnityEngine;
using System.Collections;

public class CharacterImpact : MonoBehaviour {

	public float mass = 3.0f; // defines the character mass 
	public Vector3 impact = Vector3.zero; 
	CharacterController character;
	float timeSinceImpact = 0;  // how long's passed since i got impacted
	float impactDur = 1.0f;  // how long an impact should last
	float impactDone = 0;  // time impact should be over
	
	
	void Start() { 
		character = GetComponent<CharacterController>();
		if (character == null) 
			character = GetComponentInParent<CharacterController>();
			
		if (character == null) 
			print ("crap");
		
	}
	
	void Update() { 
		// apply the impact force: 
		if (impact.magnitude > 0.2) {
			character.Move(impact * Time.deltaTime); 
			timeSinceImpact += Time.deltaTime;
			
			// consumes the impact energy each cycle:
			impact = Vector3.Lerp(impact, Vector3.zero, Util.Ease.In(timeSinceImpact / impactDur)); 
		}
	}

	
	// call this function to add an impact force: 
	public void Knockback(Vector3 dir, float force) {
		dir.Normalize(); 
		if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground 
		
		impact += dir.normalized * force / mass; 
		//impactDone = impactDur + Time.time;
	}
}
