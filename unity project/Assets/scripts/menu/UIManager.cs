﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class UIManager : MonoBehaviour {
	
	// for fading to black
	float fadeTime = .35f;
	bool fading;
	
	static UIManager _uiMgr = null;
	public static UIManager Instance {get {return _uiMgr;}}
	public Image fadeOverlay;
	
	void Awake() {
		// singleton setup
		if (_uiMgr == null) {
			DontDestroyOnLoad(transform.gameObject);
			_uiMgr = this;
		}
		else if (_uiMgr != this) {
			//Destroy (this);
		}
		
		//Cursor.lockState = CursorLockMode.Locked;
	}
	
	void OnLevelWasLoaded(int lvl) {
	
		FadeToColor(Color.white, 
		            () =>  fadeOverlay.gameObject.SetActive(false)
		            );
	}
	
	
	/// <summary>
	/// Fades given graphic to black.
	/// </summary>
	public void FadeToColor(Color32 destColor, UnityAction postFade) {
		fadeOverlay.gameObject.SetActive(true);
		StartCoroutine(_FadeTo(destColor, postFade));
	}
	
	public IEnumerator _FadeTo(Color32 destColor, UnityAction postFade) {
		if (fading) yield break;
		fading = true;
		
		Color32 col = fadeOverlay.color;
		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / fadeTime)
		{
			fadeOverlay.color = Color32.Lerp(col, destColor, t);
			yield return null;
		}
		fading = false;
		postFade();
		yield return null;
	}	
}
