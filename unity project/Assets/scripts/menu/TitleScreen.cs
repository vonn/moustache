﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class TitleScreen : MonoBehaviour {

	public void StartGame() {
	
	
		// fade to black
		UIManager.Instance.FadeToColor(Color.black, _StartGame);
	}
	
	
	void _StartGame() {
		Application.LoadLevel(Levels.Tutorial);
	}
}
