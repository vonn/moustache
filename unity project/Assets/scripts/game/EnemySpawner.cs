﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public Enemy[] enemies;
	public Transform enemiesFolder;
	public bool active;
	public float spawnCD = 15f;
	public float timeStarted = 0;
	public float duration = 60f;
	public Transform spawnPoints;
	public int minAmtToSpawn = 1;
	public int maxAmtToSpawn = 5;
	public bool waveOver = false;
	public float wimpyBuffMod = 1;

	public static EnemySpawner Instance {get; private set;}
	
	void Start() {
		if (Instance == null)
			Instance = this;
		else
			Destroy (this.gameObject);
	
		foreach (Enemy e in enemies)
			ObjectPool.CreatePool(e, 50);
	}
	
	void Update() {
		if (active) {
			if (Time.time >= timeStarted + duration) {
				//active = false;
			}
		}
	}
	
	/**/
	
	Enemy GetEnemy() {
		return enemies[Random.Range (0, enemies.Length)];
	}
	
	
	public void StartSpawner() {
		active = true;
		timeStarted = Time.time;
		StartCoroutine(SpawnerLoop());
	}
	
	IEnumerator SpawnerLoop() {
		float timeLastSpawned = 0;
		//yield return new WaitForSeconds(spawnCD);
		// spawner loop
		while (active && !GameController.Instance.IsMaxWave) {
			// tell gctrl we are on new wave
			waveOver = false;
			GameController.Instance.wave++;
			
			// INCREASE DIFFICULTY EVERY 3 WAVES
			if (GameController.Instance.wave % 3 == 0) 
				IncreaseDifficulty();
			
			int amt = (int)Random.Range (minAmtToSpawn, maxAmtToSpawn);
			for (int i = 0; i < amt; i++) {
				// keep track of last spawned time
				timeLastSpawned = Time.time;
				
				SpawnSingleEnemy();
				
				yield return new WaitForSeconds(Random.Range(0.0f, 3.0f));
			}
			
			waveOver = true;
			
			yield return new WaitForSeconds(Random.Range(spawnCD - 3.0f, spawnCD + 3.0f));
		}
		yield return null;
	}
	
	void SpawnSingleEnemy() {
		Enemy cur = ObjectPool.Spawn (GetEnemy());
		cur.target = Player.Instance.transform;
		cur.transform.position = DeterminePosition();
		cur.transform.parent = enemiesFolder;
		
		cur.Speed *= wimpyBuffMod;  // TODO tweak; increase their speed (multiplicative increase)
		//cur.Speed += wimpyBuffMod;  // alternative, linear increase
		
		// tell the gamecontroller we spawned
		GameController.Instance.numEnemies++;
	}
	
	Vector3 DeterminePosition() {
		return spawnPoints.GetChild(Random.Range(0, spawnPoints.childCount)).position;
	}
	
	void IncreaseDifficulty() {
		print ("Increasing difficulty");
	
		// increase num to spawn
		minAmtToSpawn += 4;
		minAmtToSpawn += 4;
		
		// decrease rate
		if (spawnCD > 10)
			spawnCD -= 5;
		
		// increase modifier to buff wimpies by
		wimpyBuffMod ++;
	}
}
