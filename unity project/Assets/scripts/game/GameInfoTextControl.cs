﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class GameInfoTextControl : MonoBehaviour {

	Text myText;
	public static GameInfoTextControl Instance;
	GameController gc;


	// Use this for initialization
	void Start () {
		if (Instance == null)
			Instance = this;
		else 
			Destroy (this.gameObject);
			
		myText = GetComponent<Text>();
		gc = GameController.Instance;
	}
	
	// Update is called once per frame
	void Update () {
		if (gc.state == GameState.RUNNING)
			UpdateText ();
	}
	
	void UpdateText() {	
		string timeElapsed = " fffff";
		TimeSpan t = TimeSpan.FromSeconds((Time.time - gc.timeStarted));
		
		if (t.Hours > 0) {
			timeElapsed = string.Format("{0}h {1}m {2}s", 
			                            t.Hours, 
			                            t.Minutes, 
			                            t.Seconds);
		}
		else if (t.Minutes > 0) {
			timeElapsed = string.Format("{0}m {1}s",
			                            t.Minutes, 
			                            t.Seconds);
		}
		else {
			timeElapsed = string.Format("{0}s",
			                            t.Seconds);
		}
	
		string output = string.Format("Wimpies Left:\t{0}\nTime:\t{1}\nWave:\t{2}/{3}",
		                              gc.numEnemies, timeElapsed, gc.wave, gc.waveMax);
		                              
		myText.text = output;
	}
}
