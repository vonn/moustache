﻿using UnityEngine;
using System.Collections;

public enum GameState {
	NOTSTARTED,
	TUTORIAL,
	PREGAME,
	RUNNING,
	ENDING,
	OVER,
	LIMBO
}

[System.Serializable]
public struct GameStats {
	public int score;
	public int numKills;
	public int smallWimpsKilled;
	public int bigWimpsKilled;
	public int boomerangHits;
}


public class GameController : MonoBehaviour 
{
	[SerializeField] GameObject pregameOverlay;
	[SerializeField] GameObject postgameOverlay;

	public int numEnemies = 0;
	public float timeStarted = 0;
	public int wave = 0;
	public int waveMax = 10;
	public GameState state = GameState.NOTSTARTED;
	public bool IsMaxWave { get { return wave == waveMax; } }
	
	public GameStats stats = new GameStats();
	
	bool GameOverCondition { get { return (IsMaxWave && numEnemies == 0 && spawner.waveOver) || !player.IsAlive; }	}

	public static GameController Instance;
	EnemySpawner spawner;
	Player player;
	

	// Use this for initialization
	void Start () {
		if (Instance == null) 
			Instance = this;
		else
			Destroy (this.gameObject);
			
		player = Player.Instance;
		spawner = EnemySpawner.Instance;
		pregameOverlay.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		if (state == GameState.NOTSTARTED) {
			DisplayTutorial();
			state = GameState.TUTORIAL;
		}
		else if (state == GameState.TUTORIAL) {
			// TODO
		}
		else if (state == GameState.PREGAME) {
			// TODO maybe countdown before things start spawning (ready, go!)
		}
		else if (state == GameState.RUNNING) {
			UpdateRunning();
		}
		else if (state == GameState.ENDING) {
			// idk, something else here?
			state = GameState.OVER;
		}
		else if (state == GameState.OVER) {
			//state = GameState.LIMBO;
			// clean things up
			
			// go back to main menu
		}
		else if (state == GameState.LIMBO) {
			// panic?
		}
		
		HandleInput();
	}
	
	/**/
	
	
	void HandleInput() {
		// CLICK TO START
		if (Input.GetButtonDown("Fire1")) {
			// maybe start game elsewhere
			if (state == GameState.TUTORIAL) {
				StartGame ();
			}
		}
		
		// PRESS R TO RESTART
		if (state == GameState.OVER) {
			if (Input.GetKeyDown(KeyCode.R)) {
				Application.LoadLevel(Levels.Tutorial);
			}
		}
	}
	
	
	void DisplayTutorial() {
		// show dialog on how2play game
		
	}
	
	
	void UpdateRunning() {
		// check if game should be over
		// update any game things
		if (GameOverCondition) {
			print ("game should end");
			
			if (player.IsAlive)
				GameOverVictory();
			else
				GameOverDefeat();
		}
	}
	
	
	void StartGame() {
		Util.LockCursor();
		pregameOverlay.SetActive(false);
		spawner.StartSpawner();
		timeStarted = Time.time;
		player.Controllable = true;
		
		state = GameState.RUNNING;
	}
	
	void EndGame() {
		print ("GAME ENDING?!?!?");
		state = GameState.ENDING;
		player.Controllable = false;
		
		postgameOverlay.SetActive(true);
		Util.UnlockCursor();
	}
	
	// TODO
	void GameOverVictory() {
		EndGame();
	}
	
	// TODO
	void GameOverDefeat() {
		EndGame();
	}
}
