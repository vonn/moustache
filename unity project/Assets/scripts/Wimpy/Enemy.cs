﻿using UnityEngine;
using System.Collections;

public enum EnemyState { IDLE, MOVING, AGGRO, ATTACKING, DYING, DEAD }
public enum EnemyType { SMALL, BIG }

public class Enemy : MonoBehaviour {

#region FIELDS AND PROPERTIES
	public int hp = 1;
	public EnemyState state = EnemyState.IDLE;
	public Transform target;
	[SerializeField] float _speed = 0;
	public float Speed {
		get { return _speed; }
		set {
			_speed = value;
			anim.speed = value;
		}
	}
	public EnemyType type;
	public float grav = 20;
	public float thinkCD = 3f;
	public GameObject dummyMoustache;
	public BoxCollider hitbox;
	public Animator anim;
	public bool IsAlive {
		get {
			return state != EnemyState.DEAD && state != EnemyState.DYING;
		}
	}
	public GameObject hitFX;  // played when i'm hit
	public AudioClip[] wimpSFX;  // played before they get a mo
	AudioClip WimpSFX { get { return wimpSFX[Random.Range (0, wimpSFX.Length)]; } }
	public AudioClip[] manSFX;  // played after
	AudioClip ManSFX { get { return manSFX[Random.Range (0, manSFX.Length)]; } }
	CharacterController cc;
	Vector3 vel = Vector3.zero;
	bool ReadyToThink {
		get {
			return Time.time >= thinkTime;
		}
	}
	bool stunned = false;
	float thinkTime = 0;
	AudioSource audioSrc;
#endregion
	

#region UNITY METHODS
	void OnEnable() {
		state = EnemyState.IDLE;
		hp = type == EnemyType.BIG ? 2 : 1; // TODO fix when modding enemy hp's (or adding new enemies)
		dummyMoustache.SetActive(false);
		thinkCD = type == EnemyType.BIG ? 2f : 3f;
	}

	void Start() {
		cc = GetComponent<CharacterController>();
		ObjectPool.CreatePool(hitFX, 4);
		audioSrc = GetComponent<AudioSource>();
		target = Player.Instance.transform;
		
		if (type == EnemyType.SMALL) Speed = 1.5f;
		else if (type == EnemyType.BIG) Speed = 2.5f;
	}

	void Update() {
		if (GameController.Instance.state != GameState.RUNNING) {
			vel = Vector3.zero;
			anim.SetFloat("speed", 0);
			state = EnemyState.IDLE;
			return;
		}
	
		if (ReadyToThink) {
			Think();
		}
		
		// update anim params
		anim.SetFloat("speed", vel.sqrMagnitude);
		
		HandleCurrentState();
	}
#endregion
	

#region MEMBER METHODS
	void Think() {
		thinkTime = Time.time + thinkCD;
		
		// TODO other states
		if (state == EnemyState.DYING) {
			state = EnemyState.DEAD;
			// tell gc we died
			GameController.Instance.numEnemies--;
			// Recycle me after a bit
			StartCoroutine(
				Util.RecycleTimeout(this.gameObject, 15f)
			);
		}
		else if (IsAlive){
		
			// Start chasing target
			if (target != null) {
				if (state != EnemyState.AGGRO)
					BeginChase();
				else if (state == EnemyState.AGGRO) 
					TryAttack();
			}
			else {
				// TODO implement
				//FindTarget();
			}
		}
	}
	
	void BeginChase() {
		state = EnemyState.AGGRO;
		StartCoroutine(PlayWimpSoundLoop());
	}
	
	/// <summary>
	/// Chases the target.
	/// </summary>
	/// <param name="away">If set to <c>true</c>, instead run away.</param>
	void ChaseTarget(bool away = false) {
		if (stunned) return;
		
		if (cc.isGrounded) {
			Vector3 delta = target.transform.position - transform.position;
			
			delta.y = 0;
			delta.Normalize();
			
			transform.LookAt(target.position);
			transform.forward = away ? -delta : delta;
			
			vel = delta * Speed;
		}
		
		if (away) vel *= -1;
		vel.y -= grav * Time.deltaTime;
		
		cc.Move (vel * Time.deltaTime);
	}
	
	void HandleCurrentState() {
		switch(state) {
		
		case EnemyState.IDLE:
			
			break;
			
		case EnemyState.MOVING:
			break;
			
		case EnemyState.AGGRO:
			ChaseTarget();
			break;
			
		case EnemyState.ATTACKING:
		
			break;
			
		case EnemyState.DYING:
		
			break;
			
		case EnemyState.DEAD:
			ChaseTarget (true); // walk away
			break;
		
		default:
			break;
		}
	}
	
	IEnumerator Impact(Vector3 srcPos, bool knockForward = false) {
		if (stunned) yield break;
		stunned = true;
		
		int numFrames = 10; // number of frames to be stunned for
		float kbForce = 25.0f;
		
		// which direction to kb
		Vector3 dir;
		if (srcPos != Vector3.zero)
			dir = -transform.forward;
		else {
			srcPos.y = 0;
			dir = -(srcPos - transform.position).normalized;
		}
		if (knockForward) dir = -dir;
		if (dir.y < 0) dir.y = -dir.y;
		
		GetComponent<CharacterImpact>().Knockback(dir, kbForce);
		
		// stun for 10 frames;
		yield return new WaitForSeconds(numFrames * Time.deltaTime);
		
		stunned = false;
	}
	
	public void ApplyDamage(int dmg, Vector3 srcPos, bool knockForward = false) {
		if (!IsAlive) return;
		StartCoroutine(Impact(srcPos, knockForward));
		
		hp -= dmg;
		
		if (hp <= 0) {
			GainMoustache();
			hp = 0;
		}
	}
	
	// Defeated.
	void GainMoustache() {
		if (!IsAlive) return;
		
		dummyMoustache.SetActive(!dummyMoustache.activeSelf);
		anim.SetFloat("speed", 0.0f);
		vel = Vector3.zero;
		
		// spawn fx
		GameObject fx = ObjectPool.Spawn(hitFX);
		fx.transform.parent = dummyMoustache.transform.parent;
		fx.transform.localRotation = Quaternion.identity;
		fx.transform.localPosition = Vector3.zero;
		fx.transform.localScale = Vector3.one;
		StartCoroutine(
			Util.RecycleTimeout(fx ,5)
		);
		
		Speed /= 2; // half speed idk
		state = EnemyState.DYING;
		
		// chill for a bit before walking away
		thinkTime = Time.time + (.75f * thinkCD);
		
		// tell the player you got a kill
		target.GetComponent<Player>().AwardKill(this);
		
		StartCoroutine(PlayManSound());
	}
	
	// play a man sfx after a half second
	IEnumerator PlayManSound() {
		// 
		yield return new WaitForSeconds(1.5f);
		
		audioSrc.PlayOneShot(ManSFX);
	}
	
	IEnumerator PlayWimpSoundLoop() {
		float minCD, maxCD;
		minCD = 6f;
		maxCD = 13f;
	
		while (IsAlive) {
			audioSrc.PlayOneShot(WimpSFX);
			
			// wait some time before tryin again
			yield return new WaitForSeconds(Random.Range(minCD, maxCD));
		}
	}
	
	void TryAttack() {
		// see if target is near (TODO: tweak dist value)
		float dist = Vector3.Distance(transform.position, target.position);
		//print ("try attack from " + dist);
		if (dist <= 1.6f) {
			Player p = Player.Instance;
			p.ApplyDamage(1, this);
		}
	}
#endregion
}
